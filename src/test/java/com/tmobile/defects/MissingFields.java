package com.tmobile.defects;
//com.tmobile.defects.MissingLabels
import java.io.FileInputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.tmobile.jira.BasicMethods;
import com.tmobile.jira.DBConnect;
import com.tmobile.jira.EmailAlert;

public class MissingFields {
	public static void main(String[] args)  {
		HashMap<String,String> testdata = new HashMap<String,String>();
		BasicMethods bm = new BasicMethods();
		testdata= bm.loadProperties();
		DBConnect db = new DBConnect(testdata);
		EmailAlert email = new EmailAlert(testdata);
		Connection conn =db.Connect();

		
		if(testdata.get("rcaFlag").equals("true")) {
			List<LinkedHashMap<String, String>> users = db.getDefectsInfo(conn,testdata.get("rcasql"));
			if(users !=null) {
				HashSet<String> toList = db.getToListFromUserDefects(conn,users,"creatorEmail");
				HashSet<String> ccList = db.getccListFromUserDefects(users);
				email.sendEmail(users,"Cancel Defects -missing Post RCA Analysis",toList,ccList);
			}	
			else
				System.out.println("Users list is null for RCA report");
		}
	if(testdata.get("missingFieldsFlag").equals("true")) {
			List<LinkedHashMap<String, String>> users = db.getDefectsInfo(conn,testdata.get("missingFieldssql"));
			if(users !=null) {
				HashSet<String> toList = db.getToListFromUserDefects(conn,users,"creatorEmail");
				HashSet<String> ccList = null;
				email.sendEmail(users,"Defects status - missing mandatory fields",toList,ccList);
				}
			else
				System.out.println("Users list is null for missing fields report");
		}
	if(testdata.get("returnRTTFlag").equals("true")) {
		List<LinkedHashMap<String, String>> users = db.getDefectsInfo(conn,testdata.get("returnRTTsql"));
			if(users !=null) {
				HashSet<String> toList = db.getToListFromUserDefects(conn,users,"creatorEmail");
				HashSet<String> ccList = db.getccListFromUserDefects(users);
				email.sendEmail(users,"Defects status-Return/RTT",toList,ccList);
				}
			else
				System.out.println("Users list is null for return and ready to test report");
		}
	if(testdata.get("closedDefectsrcaFlag").equals("true")) {
		List<LinkedHashMap<String, String>> users = db.getDefectsInfo(conn,testdata.get("closedDefectsrcaSql"));
			if(users !=null) {
				HashSet<String> toList = db.getToListFromUserClosedDefects(conn,users);
				HashSet<String> ccList = null;
				email.sendEmail(users,"Closed Defects -missing Post RCA Analysis",toList,ccList);
				}
			else
				System.out.println("Users list is null for return and ready to test report");
		}
	if(testdata.get("OpenDefectFlag").equals("true")) {
		List<LinkedHashMap<String, String>> users = db.getDefectsInfo(conn,testdata.get("OpenDefectSql"));
			if(users !=null) {
				HashSet<String> toList = db.getToListFromUserDefects(conn,users,"AssigneeEmail");
				HashSet<String> ccList = null;
				email.sendEmail(users,"Open Defects – Need immediate attention from Dev team",toList,ccList);
				}
			else
				System.out.println("Users list is null for return and ready to test report");
		}
	
		
	}



}
