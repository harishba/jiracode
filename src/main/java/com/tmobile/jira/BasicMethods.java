package com.tmobile.jira;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Properties;

public class BasicMethods {
	public HashMap<String,String> loadProperties()
	{
		 Properties properties = new Properties();
		    HashMap<String,String> testdata = new HashMap<String,String>();
		    try {
		    	
		    	FileInputStream inputStream = new FileInputStream(System.getProperty("user.dir")+"/src/test/resources/testdata/testconfig.properties");
		      properties.load(inputStream);
		      for (String key : properties.stringPropertyNames()) {
		    	    String value = properties.getProperty(key);
		    	    testdata.put(key, value);
		      	}
		      return testdata;
		    } catch (Exception e) {
		    	e.printStackTrace();
		    	return null;
		    	
		    }
		}

}
