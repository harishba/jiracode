package com.tmobile.jira;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class EmailAlert {
	HashMap<String,String> td = new HashMap<String,String>();
	public EmailAlert(HashMap<String,String> d)
	{
		td = d;
	}
	
	public  void sendEmail(List<LinkedHashMap<String, String>> d,String subject,HashSet<String> toList,
			HashSet<String> ccList) {
	Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", td.get("host"));
		Session session = Session.getDefaultInstance(properties);
			
		int cnt = d.size();
		System.out.println(cnt);
		System.out.println(d);
		//table header
		String thead = "<tr bgcolor=aqua>";
		 for (LinkedHashMap<String, String> el: d) {  //headers
             for (Map.Entry ele: el.entrySet()) { 
            	 String key = (String) ele.getKey();
               	 if(key.equals("AGE_FROM_CREATED") ||key.equals("AGE_FROM_UPDATED")  )
             		thead = thead+ "<th bgcolor=#ff9130> <font color=black>"+key +"</font></th>";
               	 else
                 	 thead = thead+ "<th> <font color=black>"+key +"</font></th>";
       	       } 
            	 break;
		 }      
             thead = thead + "</tr>";
             System.out.println(thead);
       String autotext = "<p style=\"border:3px; border-style:solid; border-color:#FF0000; padding: 1em;\">This is an auto generated email, for any issues, please contact Naveena Basetty, Mrinal Halder or Harish Bahekar</p>";
 	   String htmlstr = autotext + "<br><h3> <font color=green> <center>"+ subject +" </center> </font></h3>";
 	   htmlstr = htmlstr +" <TABLE Cellpadding=1 cellspacing=0 width=100% border=1> " ;
 	   htmlstr = htmlstr + thead;
 
         for (LinkedHashMap<String, String> el: d) {  //html table body 
        	 htmlstr = htmlstr + "<tr bgcolor=#FFD933>";
        	 int col=0;
                 for (Map.Entry ele: el.entrySet()) { 
                	 String value = (String) ele.getValue();
                 	 System.out.println("value :"+value);
                	 if(value !=null)
                		 if(value.toLowerCase().equals("unknown"))
                			 htmlstr = htmlstr + "<td> <font color=red> <b>"+value +"</b></font></td>";
                	 else
                		 htmlstr = htmlstr + "<td> <font color=black>"+value +"</font></td>";
                	       } 
                 htmlstr = htmlstr + "</tr>";
    		 }      
         htmlstr = htmlstr + "</table>";
         System.out.println("List :"+toList);
         try {
			  MimeMessage message = new MimeMessage(session);
			  message.setFrom(new InternetAddress(td.get("from")));
			  String to = toList.toString().replace("[","");
			  to = to.replace("]", "");
			  System.out.println("Send Email to :" +to);
			  String cc = "";
			  if(ccList !=null) {
				  cc = getCCList(ccList).toString();
				  	if(cc.length() >2) {
				  		cc=cc.toString().replace("[","");
				  		cc = cc.replace("]", "");
				  		cc = cc +","+ td.get("additionalccList");
				  	}
				  	else
				  	{
				  		System.out.println("mapping not found");
				  		cc = td.get("additionalccList");
				  	}
			  } 
			  else
				  cc =  td.get("additionalccList");
			
			  System.out.println("Send Email cc :" +cc);
			  message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to)); 
			  message.setRecipients(Message.RecipientType.CC,InternetAddress.parse(cc)); 
			  message.setSubject(subject );
 	          message.setText(htmlstr, "utf-8","html");          
 	          Transport.send(message);

		      System.out.println("Sent message successfully....");

		} catch (MessagingException mex) {
			mex.printStackTrace();

		} 
         }

	public String getCCList(HashSet<String> users) {
		FileInputStream fis;
		XSSFWorkbook wb;
		Sheet sh;
		Row row;
		Cell cell;
		HashSet<String> cclist = new HashSet();
	try {	
		fis = new FileInputStream(System.getProperty("user.dir")+"/testdata/SM_PO_Mapping.xlsx");
		wb = new XSSFWorkbook(fis);
		sh = wb.getSheet("scrum_mapping");
		int rowNum = sh.getLastRowNum();
		System.out.println("Total Rows :" + rowNum);
		  Iterator<String> it = users.iterator();
		  String name = null;
		     while(it.hasNext()){
		        name = it.next() ;
		        for(int i=1;i<rowNum;i++)
				{
		            System.out.println(name +":"+ sh.getRow(i).getCell(0).getStringCellValue().trim().toLowerCase());
					if(sh.getRow(i).getCell(0).getStringCellValue().trim().toLowerCase().equals(name.trim().toLowerCase()))
					{
						cclist.add(sh.getRow(i).getCell(2).getStringCellValue().trim().toLowerCase()); //PO
						cclist.add(sh.getRow(i).getCell(4).getStringCellValue().trim().toLowerCase()); //SM
						break;
					}	
				}
		     }
		System.out.println(cclist);
		wb.close();
		fis.close();
		return cclist.toString();
	}
	catch(Exception e)
	{
		e.printStackTrace();
		return null;
	}
		
		}
	}
		
		


