package com.tmobile.jira;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class DBConnect {
	HashMap<String,String> td = new HashMap<String,String>();
	
	public DBConnect(HashMap<String,String> t)
	{
		td = t;
	}
	
	public  Connection Connect()  {
	try 
		{
		String connectionString;
		System.out.println(td );
		connectionString = "jdbc:sqlserver://"+td.get("sqlserver")+";"
				+ "database="+td.get("db")+";authentication="+td.get("authType")+";"
				+ "user="+td.get("user")+";password="+td.get("pwd")+";encrypt=true;"
				+ "trustServerCertificate=false;" + "loginTimeout=30;";

			Connection conn = DriverManager.getConnection(connectionString);
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			if (conn != null) 
				System.out.println("Connected");
			return conn;
		}
	catch(Exception e)
	{
		e.printStackTrace();
		return null;
	}
		}
	
public Map<String, String> getDefectsData(Connection conn,String query) {	
	Map<String, String> users = new HashMap();	
	try {
	Statement stmnt = conn.createStatement();
	ResultSet resultSet = stmnt.executeQuery(query);
	LinkedHashMap<String,String> d =null;

	List<LinkedHashMap<String,String>> ls =new  ArrayList<LinkedHashMap<String,String>>();
	ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
	List<String> cols = new ArrayList<String>();
	for (int i=1;i<=resultSetMetaData.getColumnCount();i++)
		cols.add(resultSetMetaData.getColumnName(i).toString());
		while (resultSet.next()) {
		 d = new LinkedHashMap<String,String>();
		for(int i=0;i<cols.size();i++) {
				if(resultSet.getString(cols.get(i))!=null)
					d.put(cols.get(i).trim(),resultSet.getString(cols.get(i)).trim().replace(",", ""));
				else
					d.put(cols.get(i).trim(),null);
			}	
			System.out.println(d);
		    if(users.containsKey(d.get("Reporter"))) {
		    	List<String> u = new ArrayList<String>();
		    	u.add(users.get(d.get("Reporter").toString()));
		    	u.add(d.toString());
		    	users.put(d.get("Reporter"), u.toString());
		    } else {
		     users.put(d.get("Reporter"), d.toString());
			}
		}
		System.out.println(users);
		return users;
			}
	catch (Exception e) {
			e.printStackTrace();
			return users;
			
		}

	}
public List<LinkedHashMap<String, String>> getDefectsInfo(Connection conn,String query) {	
	LinkedHashMap<String, String> users = new LinkedHashMap();	
	List<LinkedHashMap<String,String>> ls =new  ArrayList<LinkedHashMap<String,String>>();
	try {
	Statement stmnt = conn.createStatement();
	ResultSet resultSet = stmnt.executeQuery(query);
	LinkedHashMap<String,String> d =null;
	ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
	List<String> cols = new ArrayList<String>();
	for (int i=1;i<=resultSetMetaData.getColumnCount();i++)
		cols.add(resultSetMetaData.getColumnName(i).toString());
		while (resultSet.next()) {
		 d = new LinkedHashMap<String,String>();
		for(int i=0;i<cols.size();i++) {
				if(resultSet.getString(cols.get(i))!=null)
					d.put(cols.get(i).trim(),resultSet.getString(cols.get(i)).trim().replace(",", ""));
				else
					d.put(cols.get(i).trim(),null);
			}	
			System.out.println(d);
			ls.add(d);
		   
		}
		System.out.println(users);
		return ls;
			}
	catch (Exception e) {
			e.printStackTrace();
			return ls;
			
		}

	}

public HashSet<String> getToListFromUserDefects(Connection conn,List<LinkedHashMap<String, String>> users,String colName) {
	HashSet<String> toLst = new HashSet<String>();
	String defList = "";
try {	 
 
        System.out.println("Defects List :"+defList);
        String query =null, userkey = null,sqlstmt =null,columnName=null;
       	userkey = "jirakey";
      	sqlstmt ="select "+ colName +" from bau.JiraIssue where jirakey in(";
       	columnName = colName;
       	for (LinkedHashMap<String, String> el: users) {  //headers
                for (Map.Entry ele: el.entrySet()) { 
               	 String key = (String) ele.getKey();
               	 if(key.toLowerCase().equals(userkey)) {
               		 String value = (String) ele.getValue();
           			defList = defList + "'" + value + "',";
               		break;
               	 } 
                }
        	}
        	System.out.println("Email To List :"+ defList);
        	query = sqlstmt +defList.substring(0, defList.length()-1)+")";
        	System.out.println("SQl :"+ query);
        	Statement stmnt = conn.createStatement();
        	ResultSet resultSet = stmnt.executeQuery(query);
   
        	while (resultSet.next()) {
        		System.out.println(resultSet.getString(columnName));
        		if(!resultSet.getString(columnName).toString().toLowerCase().equals(columnName))
        			if(!resultSet.getString(columnName).toString().toLowerCase().equals("unknown"))
        				toLst.add(resultSet.getString(columnName).toString());
   			}	
        	System.out.println("Email List :"+ toLst.size() );
       }
catch(Exception e)
{
	e.printStackTrace();
	return null;
}
return toLst;
}
public HashSet<String> getToListFromUserClosedDefects(Connection conn,List<LinkedHashMap<String, String>> users) {
	HashSet<String> toLst = new HashSet<String>();
	String defList = "";
try {	 
 
        System.out.println("Defects List :"+defList);
        String query =null, userkey = null,sqlstmt =null,columnName=null;
       	userkey = "assignee";
       	sqlstmt = "select Email from bau.jirauser where name in(";
       	columnName = "Email";
       	for (LinkedHashMap<String, String> el: users) {  //headers
                for (Map.Entry ele: el.entrySet()) { 
               	 String key = (String) ele.getKey();
               	 if(key.toLowerCase().equals(userkey)) {
               		 String value = (String) ele.getValue();
           			defList = defList + "'" + value + "',";
               		break;
               	 } 
                }
        	}
        	System.out.println("Email To List :"+ defList);
        	query = sqlstmt +defList.substring(0, defList.length()-1)+")";
        	System.out.println("SQl :"+ query);
        	Statement stmnt = conn.createStatement();
        	ResultSet resultSet = stmnt.executeQuery(query);
   
        	while (resultSet.next()) {
        		System.out.println(resultSet.getString(columnName));
        		if(!resultSet.getString(columnName).toString().toLowerCase().equals(columnName))
        			toLst.add(resultSet.getString(columnName).toString());
   			}	
        	System.out.println("Email List :"+ toLst.size() );
       }
catch(Exception e)
{
	e.printStackTrace();
	return null;
}
return toLst;
}
public HashSet<String> getccListFromUserDefects(List<LinkedHashMap<String, String>> users) {
	HashSet<String> ccLst = new HashSet<String>();
try {	 
	for (LinkedHashMap<String, String> el: users) {  //headers
        for (Map.Entry ele: el.entrySet()) { 
       	 String key = (String) ele.getKey();
       	 if(key.toLowerCase().equals("reporter")) {
       		 String value = (String) ele.getValue();
       		ccLst.add(value);
       		 break;
       	 } 
        }
	} 
        System.out.println("Users for cc list :"+ccLst);
        return ccLst;
}
catch(Exception e)
{
	e.printStackTrace();
	return null;
}

}
}


